package com.app.connectJobJobber.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.app.connectJobJobber.R;
import com.app.connectJobJobber.Volley.ApiCall;
import com.app.connectJobJobber.Volley.VolleyCallback;
import com.app.connectJobJobber.helpers.SharedHelper;
import com.app.connectJobJobber.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONObject;


public class FaqActivity extends BaseActivity {

    private WebView webView;
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        webView=(WebView)findViewById(R.id.webView);
        backButton=(ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        ApiCall.getMethod(FaqActivity.this, UrlHelper.FAQ
                + SharedHelper.getKey(FaqActivity.this, "getLanguage"), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray static_pages = response.optJSONArray("static_pages");
                JSONObject jsonObject = new JSONObject();

                String url = jsonObject.optString("page_url");
                webView.getSettings().setJavaScriptEnabled(false);
                webView.loadUrl(url);
                webView.setHorizontalScrollBarEnabled(false);
            }
        });

    }
}
