package com.app.connectJobJobber.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.app.connectJobJobber.R;
import com.app.connectJobJobber.Volley.ApiCall;
import com.app.connectJobJobber.Volley.VolleyCallback;
import com.app.connectJobJobber.helpers.Utils;
import com.google.android.gms.common.api.Api;

import org.json.JSONObject;

import java.io.File;

public class TestActivity extends BaseActivity {

    private static final int READ_REQUEST_CODE = 42;
    private static final String TAG = TestActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Button button = (Button) findViewById(R.id.file_picker);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performFileSearch();
            }
        });
    }

    /**
     * Fires an intent to spin up the "file chooser" UI and select an image.
     */
    public void performFileSearch() {

        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        //ACTION_GET_CONTENT
        //Intent.createChooser(chooseFile, "Choose a file");


        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("*/*");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                if (uri != null) {
                    Log.i(TAG, "Uri: " + uri.toString());
                    showImage(uri);
                }
            }
        }
    }

    private void showImage(Uri uri) {
        String filePath = Utils.getRealPathFromUriNew(TestActivity.this, uri);
        Log.e(TAG, "filePath: " + filePath);
        if (filePath != null) {
            ApiCall.fileUpload(TestActivity.this, new File(filePath), new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.e(TAG, "response: " + response);
                    String document = response.optString("image");
                    Log.e(TAG, "document: " + document);
                }
            });
        }
    }
}